package ru.tsc.felofyanov.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.model.IHasDateBegin;

import java.util.Comparator;

public enum DateBeginComparator implements Comparator<IHasDateBegin> {

    INSTANCE;

    @Override
    public int compare(@Nullable final IHasDateBegin iHasDateBegin, @Nullable final IHasDateBegin t1) {
        if (iHasDateBegin == null || t1 == null) return 0;
        if (iHasDateBegin.getDateBegin() == null || t1.getDateBegin() == null) return 0;
        return iHasDateBegin.getDateBegin().compareTo(t1.getDateBegin());
    }
}

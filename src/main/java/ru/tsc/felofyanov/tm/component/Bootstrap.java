package ru.tsc.felofyanov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.ICommandRepository;
import ru.tsc.felofyanov.tm.api.repository.IProjectRepository;
import ru.tsc.felofyanov.tm.api.repository.ITaskRepository;
import ru.tsc.felofyanov.tm.api.repository.IUserRepository;
import ru.tsc.felofyanov.tm.api.service.*;
import ru.tsc.felofyanov.tm.command.AbstractCommand;
import ru.tsc.felofyanov.tm.command.project.*;
import ru.tsc.felofyanov.tm.command.system.*;
import ru.tsc.felofyanov.tm.command.task.*;
import ru.tsc.felofyanov.tm.command.taskproject.BindTaskToProjectCommand;
import ru.tsc.felofyanov.tm.command.taskproject.UnbindTaskFromProjectCommand;
import ru.tsc.felofyanov.tm.command.user.*;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.felofyanov.tm.exception.system.CommandNotSupportedException;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.model.User;
import ru.tsc.felofyanov.tm.repository.CommandRepository;
import ru.tsc.felofyanov.tm.repository.ProjectRepository;
import ru.tsc.felofyanov.tm.repository.TaskRepository;
import ru.tsc.felofyanov.tm.repository.UserRepository;
import ru.tsc.felofyanov.tm.service.*;
import ru.tsc.felofyanov.tm.util.DateUtil;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new HelpCommand());
        registry(new AboutCommand());
        registry(new CloseCommand());
        registry(new ShowCommand());
        registry(new ShowArgumentCommand());

        registry(new TaskListCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());

        registry(new ProjectListCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectCompeteByIdCommand());
        registry(new ProjectCompeteByIndexCommand());
        registry(new UnbindTaskFromProjectCommand());
        registry(new BindTaskToProjectCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserViewProfileCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveCommand());
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("** TASK-MANAGER SHUTTING DOWN**")));
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) System.exit(0);

        initLogger();
        initData();

        while (true) {
            try {
                System.out.println("ENTER COMMAND");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.err.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initData() {
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);
        @NotNull final User test = userService.create("test", "test", Role.USUAL);

        taskService.add(test.getId(), new Task("DEMO TASK", Status.IN_PROGRESS, DateUtil.toDate("06.05.2019")));
        taskService.add(test.getId(), new Task("TEST TASK", Status.NOT_STARTED, DateUtil.toDate("03.01.2017")));
        taskService.add(test.getId(), new Task("MEGA TASK", Status.COMPLETED, DateUtil.toDate("08.07.2021")));
        taskService.add(test.getId(), new Task("BEST TASK", Status.IN_PROGRESS, DateUtil.toDate("05.05.2018")));

        projectService.add(test.getId(), new Project("DEMO PROJECT", Status.NOT_STARTED, DateUtil.toDate("05.06.2019")));
        projectService.add(test.getId(), new Project("TEST PROJECT", Status.IN_PROGRESS, DateUtil.toDate("03.01.2017")));
        projectService.add(test.getId(), new Project("MEGA PROJECT", Status.COMPLETED, DateUtil.toDate("08.07.2021")));
        projectService.add(test.getId(), new Project("BEST PROJECT", Status.IN_PROGRESS, DateUtil.toDate("05.05.2018")));
    }


    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }
}

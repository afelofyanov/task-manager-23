package ru.tsc.felofyanov.tm.exception.system;

import ru.tsc.felofyanov.tm.exception.AbstractExcception;

public final class AccessDeniedException extends AbstractExcception {
    public AccessDeniedException() {
        super("ERROR! Access denied....");
    }
}
